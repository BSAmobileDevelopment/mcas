package com.example.reyanthonypaano.mcas.branchInquiryPkg;

public class LoanCalculatorTable {
    private String Term;
    private String MA;
    private String PNAmount;

    public LoanCalculatorTable(String term, String MA, String PNAmount) {
        this.Term = term;
        this.MA = MA;
        this.PNAmount = PNAmount;
    }

    public String getTerm() {
        return Term;
    }

    public void setTerm(String term) {
        Term = term;
    }

    public String getMA() {
        return MA;
    }

    public void setMA(String MA) {
        this.MA = MA;
    }

    public String getPNAmount() {
        return PNAmount;
    }

    public void setPNAmount(String PNAmount) {
        this.PNAmount = PNAmount;
    }
}

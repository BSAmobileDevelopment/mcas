package com.example.reyanthonypaano.mcas.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.reyanthonypaano.mcas.Commons.NavDrawerBaseActivity;
import com.example.reyanthonypaano.mcas.R;

public class TestActivity extends NavDrawerBaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_dashboard);
        displayDrawer();
        super.onCreate(savedInstanceState);
    }
}

package com.example.reyanthonypaano.mcas.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.reyanthonypaano.mcas.R;
import com.example.reyanthonypaano.mcas.adapters.InquiryListAdapter;
import com.example.reyanthonypaano.mcas.model.Customers;
import com.example.reyanthonypaano.mcas.model.Inquiries;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InquiriesTabFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InquiriesTabFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InquiriesTabFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public InquiriesTabFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InquiriesTabFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InquiriesTabFragment newInstance(String param1, String param2) {
        InquiriesTabFragment fragment = new InquiriesTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inquiries_tab, container,false);

        ListView inquiries = view.findViewById(R.id.lvInquiries);

        Inquiries john = new Inquiries("John","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries steve = new Inquiries("Steve","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries stacy = new Inquiries("Stacy","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries ashley = new Inquiries("Ashley","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt = new Inquiries("Matt","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt2 = new Inquiries("Matt2","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt3 = new Inquiries("Matt3","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt4 = new Inquiries("Matt4","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt5 = new Inquiries("Matt5","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt6 = new Inquiries("Matt6","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt7 = new Inquiries("Matt7","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt8 = new Inquiries("Matt8","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt9 = new Inquiries("Matt9","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt10 = new Inquiries("Matt10","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        Inquiries matt11 = new Inquiries("Matt11","098173","12-20-1998","BRANCH", "12-20-1998","12 Days");
        // Inflate the layout for this fragment

        //Add the Person objects to an ArrayList
        ArrayList<Inquiries> inquiryList = new ArrayList<>();
        inquiryList.add(john);
        inquiryList.add(steve);
        inquiryList.add(stacy);
        inquiryList.add(ashley);
        inquiryList.add(matt);
        inquiryList.add(matt2);
        inquiryList.add(matt3);
        inquiryList.add(matt4);
        inquiryList.add(matt5);
        inquiryList.add(matt6);
        inquiryList.add(matt7);
        inquiryList.add(matt8);
        inquiryList.add(matt9);
        inquiryList.add(matt10);
        inquiryList.add(matt11);

        InquiryListAdapter adapter = new InquiryListAdapter (getActivity(), R.layout.inquiries_listview_layout, inquiryList);
        inquiries.setAdapter(adapter);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

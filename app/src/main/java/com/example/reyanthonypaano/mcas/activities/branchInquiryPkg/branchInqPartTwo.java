package com.example.reyanthonypaano.mcas.activities.branchInquiryPkg;


import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.reyanthonypaano.mcas.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class branchInqPartTwo extends Fragment{

    private static final String TAG = "branchInqPartTwo";
    public Spinner myBiSpnners;

    public static final branchInqPartTwo newInstance(String message){
        branchInqPartTwo fragment = new branchInqPartTwo();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }
    public branchInqPartTwo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_branch_inq_part_two, container, false);

        TypedArray spnnrs = getResources().obtainTypedArray(R.array.mySpnnersId_partTwo);
        changeSpinnersLayout(spnnrs,view);
        return view;
    }

    public void changeSpinnersLayout(TypedArray spinner_name,View v){
        for (int i = 0; i < spinner_name.length(); i++){
            myBiSpnners = v.findViewById(spinner_name.getResourceId(i,0));
            ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(),R.array.select, R.layout.spinner_layout);
            adapter.setDropDownViewResource(R.layout.spinner_layout);
            myBiSpnners.setAdapter(adapter);
        }
        spinner_name.recycle();
    }

}

package com.example.reyanthonypaano.mcas.activities.branchInquiryPkg;

import android.app.DatePickerDialog;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.reyanthonypaano.mcas.R;

import java.util.Calendar;

public class branchInqPartOne extends Fragment implements View.OnClickListener{
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";

    private EditText etBirthDate;
    public Spinner myBiSpnners;
    private DatePickerDialog.OnDateSetListener onDateSetListener;

    public static final branchInqPartOne newInstance(String message) {
        branchInqPartOne fragment = new branchInqPartOne();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_MESSAGE, message);
        fragment.setArguments(bundle);
        return fragment;
    }
    public branchInqPartOne() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        String message = getArguments().getString(EXTRA_MESSAGE);
        View view = inflater.inflate(R.layout.fragment_branch_inq_part_one,container,false);
//        TextView messageTextView = view.findViewById(R.id.ExampleTextView);
//        messageTextView.setText(message);

        etBirthDate = view.findViewById(R.id.et_bi_date_birth);
        etBirthDate.setOnClickListener(branchInqPartOne.this);

        onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = month + "/" + dayOfMonth + "/" + year;
                etBirthDate.setText(date);
            }
        };

        TypedArray spnnrs = getResources().obtainTypedArray(R.array.mySpnnersId_partOne);
        changeSpinnersLayout(spnnrs,view);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.et_bi_date_birth:
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        android.R.style.Theme_Material_Light_Dialog,onDateSetListener,year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
                break;
            default:
                break;

        }
    }

    public void changeSpinnersLayout(TypedArray spinner_name,View v){
        for (int i = 0; i < spinner_name.length(); i++){
            myBiSpnners = v.findViewById(spinner_name.getResourceId(i,0));
            ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(),R.array.select, R.layout.spinner_layout);
            adapter.setDropDownViewResource(R.layout.spinner_layout);
            myBiSpnners.setAdapter(adapter);
        }
        spinner_name.recycle();
    }
}

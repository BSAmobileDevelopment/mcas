package com.example.reyanthonypaano.mcas;

import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.reyanthonypaano.mcas.adapters.NetworkPopWindowAdapter;
import com.example.reyanthonypaano.mcas.fragments.Network_pop_window_all;
import com.example.reyanthonypaano.mcas.fragments.Network_pop_window_login;

import java.util.ArrayList;
import java.util.List;

public class NetworkPopUp extends FragmentActivity{
    private Button btn;
    private static boolean onLogin;
    private static final String TAG = "NetworkPopUp";

    NetworkPopWindowAdapter networkPopWindowAdapter;
    ViewPager viewPager;
    public static boolean isOnLogin() {
        return onLogin;
    }

    public void setOnLogin(boolean onLogin) {
        this.onLogin = onLogin;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.network_pop_window_main);

        List<Fragment> fragments = getFragements();
        networkPopWindowAdapter = new NetworkPopWindowAdapter(getSupportFragmentManager(),fragments);
        viewPager = findViewById(R.id.NetworkSlider);

        viewPager.setAdapter(networkPopWindowAdapter);
        if(isOnLogin()){
            viewPager.setCurrentItem(getItem(+0),true);
        }else
        {
            viewPager.setCurrentItem(getItem(+1),true);
        }

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.dimAmount = 0.75f;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getWindow().setAttributes(layoutParams);
        getWindow().setLayout((int)(width * .8),(int)(height * .18));

        Log.d("testing","Login nga ba? :"+isOnLogin());

    }

    @Override
    public boolean onKeyDown(int keyCode , KeyEvent event){
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            Toast.makeText(NetworkPopUp.this, "onkeydown", Toast.LENGTH_SHORT).show();
            Log.d(this.getClass().getName(), "back button pressed");
            popWindow();

            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void popWindow(){
        try {
            if(!Utils.checkNetworkInit(NetworkPopUp.this)){
                Log.d("Network_pop_window_all"," condition: " + Utils.checkNetworkInit(NetworkPopUp.this));
                NetworkJobScheduler.showPopWindow(NetworkPopUp.this);
            }else {
                NetworkJobScheduler.scheduleJob(NetworkPopUp.this);
                Log.d("Network_pop_window_all","finish");
            }
        }finally {
            finish();
            Log.d("Network_pop_window_all","finish trycatch");
        }
    }
    private List<Fragment> getFragements(){
        List<Fragment> fList = new ArrayList<>();

        fList.add(Network_pop_window_login.newInstance());
        fList.add(Network_pop_window_all.newInstance());

        return fList;
    }

    private int getItem(int i){
        return viewPager.getCurrentItem() + i;
    }
}

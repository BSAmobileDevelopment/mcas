package com.example.reyanthonypaano.mcas;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class Login extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "Login";
    private Integer schedulerId = 001;
    private Button btnMode;
    private boolean isNetworkWorking;
    NetworkJobService networkJobService = new NetworkJobService();
    NetworkPopUp networkPopUp = new NetworkPopUp();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        initControls();
        setContentView(R.layout.activity_login);

        btnMode = findViewById(R.id.btnOnline);
        btnMode.setOnClickListener(Login.this);

        btnMode = findViewById(R.id.btnOffline);
        btnMode.setOnClickListener(Login.this);

        btnMode = findViewById(R.id.btnLogin);
        btnMode.setOnClickListener(Login.this);

        networkJobService.setOnlineMode(true);
        isNetworkWorking = Utils.checkNetworkInit(Login.this);
        Mode(isNetworkWorking,Login.this);


        networkPopUp.setOnLogin(true); // if login is success set to false
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnOnline:
                Mode(true,Login.this);
                break;
            case R.id.btnOffline:
                Mode(false,Login.this);
                break;
            case R.id.btnLogin:
                doLogin();
                break;
        }
    }

    private void Mode(Boolean isOnlineMode,Context context) {
        if(isOnlineMode){
            networkJobService.setOnlineMode(true);
            btnSettings(true);
            Utils.checkNetwork(context,false);
        }else{
            networkJobService.setOnlineMode(false);
            NetworkJobScheduler.cancelJob(Login.this);
            btnSettings(false);
            Toast.makeText(context, "OffLine Mode", Toast.LENGTH_SHORT).show();
        }
    }
    private void btnSettings(Boolean setUp)
    {
        Button btnOfflineMode = findViewById(R.id.btnOffline);
        Button btnOnlineMode = findViewById(R.id.btnOnline);

        if( setUp )
        {
            btnOnlineMode.setEnabled(false);
            btnOfflineMode.setEnabled(true);
        }else
        {
            btnOnlineMode.setEnabled(true);
            btnOfflineMode.setEnabled(false);
        }
    }


    private void doLogin(){
        networkPopUp.setOnLogin(false); // if login is success set to false
    }
}

package com.example.reyanthonypaano.mcas.activities.CADPkg;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.reyanthonypaano.mcas.Commons.NavDrawerBaseActivity;
import com.example.reyanthonypaano.mcas.R;
import com.example.reyanthonypaano.mcas.activities.branchInquiryPkg.BranchInqAdapter;
import com.example.reyanthonypaano.mcas.activities.branchInquiryPkg.BranchInquiry;

import java.util.ArrayList;
import java.util.List;

public class CAD extends NavDrawerBaseActivity
    implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener{

    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private FloatingActionButton btnNextPrev;
    private int dotsCount;
    private ImageView[] dots;
    private Integer moveToNextPageCount = 0;

    CADAdapter cadAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cad_nav);
        displayDrawer();

        List<Fragment> fragments = getFragments();
        sliderDotspanel = findViewById(R.id.CADSlider);
        cadAdapter = new CADAdapter(getSupportFragmentManager(),fragments);
        viewPager = findViewById(R.id.cadPager);
        viewPager.setAdapter(cadAdapter);

        dotsCount = cadAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++){
            dots[i] = new ImageView(CAD.this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.nonactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8,0,8,0);

            sliderDotspanel.addView(dots[i],params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.active_dot));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for(int i =0; i < dotsCount; i++)
                {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.nonactive_dot));

                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        showPreviousbtn();
        //Start of OnClick Events
        btnNextPrev = findViewById(R.id.float_btn_bi_next_slide);
        btnNextPrev.setOnClickListener(CAD.this);

        btnNextPrev = findViewById(R.id.float_btn_bi_prev_slider);
        btnNextPrev.setOnClickListener(CAD.this);

    }

    private  List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<>();

        fList.add(CADPart1.newInstance(" Frag1"));
        fList.add(CADPart2.newInstance(" Frag2"));

        return fList;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.float_btn_bi_next_slide:
                viewPager.setCurrentItem(getItem(+1),true);
                showPreviousbtn();
                Toast.makeText(CAD.this, "Current Item"+viewPager.getCurrentItem(), Toast.LENGTH_LONG).show();
                break;
            case R.id.float_btn_bi_prev_slider:
                viewPager.setCurrentItem(getItem(-1),true);
                showPreviousbtn();
                Toast.makeText(CAD.this, "Current Item"+viewPager.getCurrentItem(), Toast.LENGTH_LONG).show();
                break;
            default:
                break;

        }

    }

    private int getItem(int i){
        moveToNextPageCount = viewPager.getCurrentItem() + i;
        return viewPager.getCurrentItem() + i;
    }

    public void showPreviousbtn(){
        FloatingActionButton btnPrev = findViewById(R.id.float_btn_bi_prev_slider);
        FloatingActionButton btnNext = findViewById(R.id.float_btn_bi_next_slide);
        FloatingActionButton btnSave = findViewById(R.id.float_btn_bi_save_records);

        if ( moveToNextPageCount == 0 ){
            btnPrev.hide();
            btnNext.show();

            btnSave.hide();
        }else
        {
            btnPrev.show();
            btnNext.show();

            btnSave.hide();
        }
        if(moveToNextPageCount == 4){
            btnNext.hide();
            btnPrev.show();

            btnSave.show();
        }

    }
}

package com.example.reyanthonypaano.mcas.model;

public class Inquiries {

    private String name;
    private String inquiryId;
    private String inquiryDate;
    private String branch;
    private String birthday;
    private String ageing;

    public Inquiries(String name, String inquiryId, String inquiryDate, String branch, String birthday, String ageing) {
        this.name = name;
        this.inquiryId = inquiryId;
        this.inquiryDate = inquiryDate;
        this.branch = branch;
        this.birthday = birthday;
        this.ageing = ageing;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInquiryId() {
        return inquiryId;
    }

    public void setInquiryId(String inquiryId) {
        this.inquiryId = inquiryId;
    }

    public String getInquiryDate() {
        return inquiryDate;
    }

    public void setInquiryDate(String inquiryDate) {
        this.inquiryDate = inquiryDate;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAgeing() {
        return ageing;
    }

    public void setAgeing(String ageing) {
        this.ageing = ageing;
    }
}

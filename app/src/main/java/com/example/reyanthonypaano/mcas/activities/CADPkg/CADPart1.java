package com.example.reyanthonypaano.mcas.activities.CADPkg;

import android.app.DatePickerDialog;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.reyanthonypaano.mcas.R;
import com.example.reyanthonypaano.mcas.activities.branchInquiryPkg.branchInqPartOne;

import java.util.Calendar;

public class CADPart1 extends Fragment implements View.OnClickListener{
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";

    private EditText etBirthDate;
    public Spinner myBiSpnners;
    private DatePickerDialog.OnDateSetListener onDateSetListener;

    public static final CADPart1 newInstance(String message) {
        CADPart1 fragment = new CADPart1();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_MESSAGE, message);
        fragment.setArguments(bundle);
        return fragment;
    }
    public CADPart1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        String message = getArguments().getString(EXTRA_MESSAGE);
        View view = inflater.inflate(R.layout.frag_cad_p1, container,false);
//        TextView messageTextView = view.findViewById(R.id.ExampleTextView);
//        messageTextView.setText(message);


        TypedArray spnnrs = getResources().obtainTypedArray(R.array.mySpnnersCAD_partOne);
        changeSpinnersLayout(spnnrs,view);
        return view;
    }


    public void changeSpinnersLayout(TypedArray spinner_name,View v){
        for (int i = 0; i < spinner_name.length(); i++){
            myBiSpnners = v.findViewById(spinner_name.getResourceId(i,0));
            ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(),R.array.select, R.layout.spinner_layout);
            adapter.setDropDownViewResource(R.layout.spinner_layout);
            myBiSpnners.setAdapter(adapter);
        }
        spinner_name.recycle();
    }

    @Override
    public void onClick(View view) {

    }
}

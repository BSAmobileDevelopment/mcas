package com.example.reyanthonypaano.mcas.activities;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.example.reyanthonypaano.mcas.Login;
import com.example.reyanthonypaano.mcas.OnClearFromRecentService;
import com.example.reyanthonypaano.mcas.R;
import com.example.reyanthonypaano.mcas.activities.branchInquiryPkg.BranchInquiry;

public class SplashScreenActivity extends AppCompatActivity {

    private int SLEEP_TIMER = 1;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        startService(new Intent(getBaseContext(), OnClearFromRecentService.class));
        setContentView(R.layout.activity_splash_screen);
        getSupportActionBar().hide();
        SplashScreen splashScreen = new SplashScreen();
        splashScreen.start(); // Runs an instance of SplashScreen

    }

    private class SplashScreen extends Thread {
        public void run(){
            try{
                sleep(1000 * SLEEP_TIMER);
                progressBar = findViewById(R.id.splashProgressBar);
                progressBar.setMax(100);

            }catch (InterruptedException e){
                e.printStackTrace();

            }

            Intent intent = new Intent(SplashScreenActivity.this, Login.class);
            startActivity(intent);
            SplashScreenActivity.this.finish();
        }
    }
}
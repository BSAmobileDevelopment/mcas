package com.example.reyanthonypaano.mcas.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.reyanthonypaano.mcas.model.Customers;

import java.util.ArrayList;

public class SpinAdapter extends ArrayAdapter<Customers> {

    private Context mContext;
    private ArrayList<Customers> values;

    public SpinAdapter(Context context, int textViewResourceId, ArrayList<Customers> values){
        super(context, textViewResourceId, values);
        this.mContext = context;
        this.values = values;

    }

    @Override
    public Customers getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(values.get(position).getCustMame());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getCustMame());

        return label;
    }
}

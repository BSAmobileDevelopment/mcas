package com.example.reyanthonypaano.mcas.activities.branchInquiryPkg;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.reyanthonypaano.mcas.Commons.NavDrawerBaseActivity;
import com.example.reyanthonypaano.mcas.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BranchInquiry extends NavDrawerBaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener{
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private FloatingActionButton btnNextPrev;
    private int dotsCount;
    private ImageView[] dots;
    private DatePickerDialog.OnDateSetListener onDateSetListener;
    private TextView txtInqDate;
    private Integer moveToNextPageCount = 0;
    //new
    BranchInqAdapter branchInqAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //new
        setContentView(R.layout.activity_bi);
        displayDrawer();

        List<Fragment> fragments = getFragements();
        sliderDotspanel = findViewById(R.id.branchInqSlider);
        branchInqAdapter = new BranchInqAdapter(getSupportFragmentManager(),fragments);
        viewPager = findViewById(R.id.branchInqPager);
        viewPager.setAdapter(branchInqAdapter);

        dotsCount = branchInqAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++){
            dots[i] = new ImageView(BranchInquiry.this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.nonactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8,0,8,0);

            sliderDotspanel.addView(dots[i],params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.active_dot));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for(int i =0; i < dotsCount; i++)
                {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.nonactive_dot));

                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.active_dot));
                Toast.makeText(BranchInquiry.this, "Page part"+dots[position], Toast.LENGTH_LONG).show();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        showPreviousbtn();
        //Start of OnClick Events
        btnNextPrev = findViewById(R.id.float_btn_bi_next_slide);
        btnNextPrev.setOnClickListener(BranchInquiry.this);

        btnNextPrev = findViewById(R.id.float_btn_bi_prev_slider);
        btnNextPrev.setOnClickListener(BranchInquiry.this);

        txtInqDate = findViewById(R.id.tv_bi_inq_date);
        txtInqDate.setOnClickListener(BranchInquiry.this);

        onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = month + "/" + dayOfMonth + "/" + year;
                txtInqDate.setText(date);
            }
        };
        //end of onclick Events

    }

    private List<Fragment> getFragements(){
        List<Fragment> fList = new ArrayList<>();

        fList.add(branchInqPartOne.newInstance(" Frag1"));
        fList.add(branchInqPartTwo.newInstance(" Frag2"));
        fList.add(branchInqPartThree.newInstance(" Frag3"));
//        fList.add(branchInqPartOne.newInstance(" Frag4"));



        return fList;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_bi_inq_date:
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(BranchInquiry.this,
                        android.R.style.Theme_Material_Light_Dialog,onDateSetListener,year,month,day);

                dialog.getDatePicker().setSpinnersShown(true);
                dialog.getDatePicker().setCalendarViewShown(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

                dialog.show();
                break;
            case R.id.float_btn_bi_next_slide:
                viewPager.setCurrentItem(getItem(+1),true);
                showPreviousbtn();
                Toast.makeText(BranchInquiry.this, "Current Item"+viewPager.getCurrentItem(), Toast.LENGTH_LONG).show();
                break;
            case R.id.float_btn_bi_prev_slider:
                viewPager.setCurrentItem(getItem(-1),true);
                showPreviousbtn();
                Toast.makeText(BranchInquiry.this, "Current Item"+viewPager.getCurrentItem(), Toast.LENGTH_LONG).show();
                break;
            default:
                break;

        }
    }

    private int getItem(int i){
        moveToNextPageCount = viewPager.getCurrentItem() + i;
        return viewPager.getCurrentItem() + i;
    }

    public void showPreviousbtn(){
        FloatingActionButton btnPrev = findViewById(R.id.float_btn_bi_prev_slider);
        FloatingActionButton btnNext = findViewById(R.id.float_btn_bi_next_slide);
        FloatingActionButton btnSave = findViewById(R.id.float_btn_bi_save_records);

        if ( moveToNextPageCount == 0 ){
            btnPrev.hide();
            btnNext.show();

            btnSave.hide();
        }else
        {
            btnPrev.show();
            btnNext.show();

            btnSave.hide();
        }
        if(moveToNextPageCount == 4){
            btnNext.hide();
            btnPrev.show();

            btnSave.show();
        }

    }

}

package com.example.reyanthonypaano.mcas.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.reyanthonypaano.mcas.R;
import com.example.reyanthonypaano.mcas.fragments.CustomersTabFragment;
import com.example.reyanthonypaano.mcas.model.Customers;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class CustomerListAdapter extends ArrayAdapter<Customers> {

    private Context mContext;
    private int mResource;
    private int lastPosition = -1;

    static class ViewHolder {
        TextView name;
        TextView ecmastId;
        TextView birthdate;
    }

    public CustomerListAdapter(FragmentActivity context, int resource, ArrayList<Customers> objects) {
        super(context,resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        String name = getItem(position).getCustMame();
        String ecmastId = getItem(position).getEcmastId();
        String birthday = getItem(position).getBirthDate();
        //Create the Customer Object with the information

        Customers customers = new Customers(name, birthday, ecmastId);

        //showing the animation
        final View result;

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);

            holder = new ViewHolder();
            holder.name =  convertView.findViewById(R.id.tvCustName);
            holder.birthdate = convertView.findViewById(R.id.tvBirthDate2);
            holder.ecmastId = convertView.findViewById(R.id.tvEcmastId);

            result = convertView;
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.loading_down_anim : R.anim.loading_up_anim);

        result.startAnimation(animation);
        lastPosition = position;

        holder.name.setText(name);
        holder.ecmastId.setText(ecmastId);
        holder.birthdate.setText(birthday);

//        TextView tvName = convertView.findViewById(R.id.textView1);
//        TextView tvBirthdate = convertView.findViewById(R.id.textView2);
//        TextView tvSex= convertView.findViewById(R.id.textView3);
//
//        tvName.setText(name);
//        tvBirthdate.setText(birthday);
//        tvSex.setText(sex);

        return convertView;
    }
}

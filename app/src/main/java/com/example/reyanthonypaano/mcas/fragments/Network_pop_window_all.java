package com.example.reyanthonypaano.mcas.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.reyanthonypaano.mcas.NetworkJobScheduler;
import com.example.reyanthonypaano.mcas.NetworkJobService;
import com.example.reyanthonypaano.mcas.NetworkPopUp;
import com.example.reyanthonypaano.mcas.R;
import com.example.reyanthonypaano.mcas.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class Network_pop_window_all extends Fragment implements View.OnClickListener{
    Button button;

    public static final Network_pop_window_all newInstance(){
        Network_pop_window_all fragment = new Network_pop_window_all();
        Bundle  bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    public Network_pop_window_all() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_network_pop_window_all, container, false);

        button = view.findViewById(R.id.btn_ReLogin);
        button.setOnClickListener(Network_pop_window_all.this);
        button = view.findViewById(R.id.btn_ContinueOnlineMode);
        button.setOnClickListener(Network_pop_window_all.this);
        return view;
    }

    @Override
    public void onClick(View v) {
        NetworkPopUp networkPopUp = new NetworkPopUp();
        switch (v.getId())
        {
            case R.id.btn_ReLogin:
                NetworkJobScheduler.reLogin(getActivity());
                getActivity().finish();

                networkPopUp.finish();
                break;
            case R.id.btn_ContinueOnlineMode:
                try {
                    if(!Utils.checkNetworkInit(getActivity())){
                        Log.d("Network_pop_window_all"," condition: " + Utils.checkNetworkInit(getActivity()));
                        NetworkJobScheduler.showPopWindow(getActivity());
                    }else {
                        NetworkJobScheduler.scheduleJob(getActivity());
                        Log.d("Network_pop_window_all","finish");
                    }
                }finally {
                    getActivity().finish();
                    networkPopUp.finish();
                    Log.d("Network_pop_window_all","finish trycatch");
                }
                break;
        }
    }
}

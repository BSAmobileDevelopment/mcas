package com.example.reyanthonypaano.mcas;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.util.Log;

public class Utils {
    private static final String TAG = "Utils";

    //check network connection
    public static boolean checkNetworkInit(Context context) {
        NetworkJobService.resetStaticVariables(false);

        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());

    }

    public static void checkNetwork(Context context, boolean inPopWindow) {
        NetworkJobService.resetStaticVariables(false);
//        Log.d("checkifLogin",":123123sample :" + context.getClass().getSimpleName());
//        NetworkJobService.resetStaticVariables(false);
        boolean isNetworkWorking = checkNetworkInit(context);

        if ( isNetworkWorking ){
            NetworkJobScheduler.scheduleJob(context);
        }else {
            if (!inPopWindow){
                NetworkJobScheduler.showPopWindow(context);
                Log.d(TAG,"condition 1");
            }
        }
    }

    //check network if mobile or wifi
    public static String networkType(Context context){
        String type = null;
        ConnectivityManager connMgr =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isWifiConn = false;
        boolean isMobileConn = false;
        for (Network network : connMgr.getAllNetworks()) {
            NetworkInfo networkInfo = connMgr.getNetworkInfo(network);
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                isWifiConn |= networkInfo.isConnected();

            }
            if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                isMobileConn |= networkInfo.isConnected();
            }
        }

        if ( isWifiConn && !isMobileConn){
            type = "wifi";
        }else if ( !isMobileConn && !isWifiConn){
            type = "mobile";
        }else if ( isMobileConn && isWifiConn ){
            type = "bothActive";
        }else if ( !isMobileConn && !isWifiConn ){
            type = "bothInactive";
        }

        Log.d(TAG, "Wifi connected: " + isWifiConn);
        Log.d(TAG, "Mobile connected: " + isMobileConn);

        return type;
    }
}

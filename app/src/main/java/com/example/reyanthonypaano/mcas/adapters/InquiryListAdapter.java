package com.example.reyanthonypaano.mcas.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.reyanthonypaano.mcas.R;
import com.example.reyanthonypaano.mcas.model.Customers;
import com.example.reyanthonypaano.mcas.model.Inquiries;

import java.util.ArrayList;

public class InquiryListAdapter extends ArrayAdapter<Inquiries> {

    private Context mContext;
    private int mResource;
    private int lastPosition = -1;

    static class ViewHolder{
        TextView name;
        TextView inquiryId;
        TextView inquiryDate;
        TextView branch;
        TextView birthDate;
        TextView ageing;


    }
    public InquiryListAdapter(FragmentActivity context, int resource, ArrayList<Inquiries> objects) {
        super(context,resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String name = getItem(position).getName();
        String inquiryId = getItem(position).getInquiryId();
        String inquiryDate = getItem(position).getInquiryDate();
        String branch = getItem(position).getBranch();
        String birthDate = getItem(position).getBirthday();
        String ageing = getItem(position).getAgeing();

        //Create the Inquiry Object with the information

        Inquiries inquiries = new Inquiries(name, inquiryId, inquiryDate, branch, birthDate, ageing);

        final View result;

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);

            holder = new ViewHolder();
            holder.name = convertView.findViewById(R.id.tvName);
            holder.inquiryId = convertView.findViewById(R.id.tvInquiryId);
            holder.inquiryDate = convertView.findViewById(R.id.tvInquiryDate);
            holder.branch = convertView.findViewById(R.id.tvBranch);
            holder.birthDate = convertView.findViewById(R.id.tvBirthDate);
            holder.ageing = convertView.findViewById(R.id.tvAeging);

            result = convertView;
            convertView.setTag(holder);

        }
        else {
            holder = (ViewHolder) convertView.getTag();
            result = convertView;

        }

        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position >lastPosition) ? R.anim.loading_down_anim : R.anim.loading_up_anim);

        result.startAnimation(animation);
        lastPosition = position;

        holder.name.setText(name);
        holder.inquiryId.setText(inquiryId);
        holder.inquiryDate.setText(inquiryDate);
        holder.branch.setText(branch);
        holder.birthDate.setText(birthDate);
        holder.ageing.setText(ageing);

        return convertView;
    }
}

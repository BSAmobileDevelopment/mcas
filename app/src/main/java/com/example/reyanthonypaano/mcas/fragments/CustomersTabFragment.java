package com.example.reyanthonypaano.mcas.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.reyanthonypaano.mcas.R;
import com.example.reyanthonypaano.mcas.adapters.CustomerListAdapter;
import com.example.reyanthonypaano.mcas.model.Customers;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InquiriesTabFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InquiriesTabFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CustomersTabFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CustomersTabFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InquiriesTabFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CustomersTabFragment newInstance(String param1, String param2) {
        CustomersTabFragment fragment = new CustomersTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_customers_tab, container, false);

        ListView customerNames = view.findViewById(R.id.lvCustomer);

        Customers john = new Customers("John","09778738263","12-20-1998");
        Customers steve = new Customers("Steve","09778738263","08-03-1987");
        Customers stacy = new Customers("Stacy","09778738263","11-15-2000");
        Customers ashley = new Customers("Ashley","09778738263","07-02-1999");
        Customers matt = new Customers("Matt","09778738263","03-29-2001");
        Customers matt2 = new Customers("Matt2","09778738263","03-29-2001");
        Customers matt3 = new Customers("Matt3","09778738263","03-29-2001");
        Customers matt4 = new Customers("Matt4","09778738263","03-29-2001");
        Customers matt5 = new Customers("Matt5","09778738263","03-29-2001");
        Customers matt6 = new Customers("Matt6","09778738263","03-29-2001");
        Customers matt7 = new Customers("Matt7","09778738263","03-29-2001");
        Customers matt8 = new Customers("Matt8","09778738263","03-29-2001");
        Customers matt9 = new Customers("Matt9","09778738263","03-29-2001");
        Customers matt10 = new Customers("Matt10","09778738263","03-29-2001");
        Customers matt11 = new Customers("Matt11","09778738263","03-29-2001");

        //Add the Person objects to an ArrayList
        ArrayList<Customers> peopleList = new ArrayList<>();
        peopleList.add(john);
        peopleList.add(steve);
        peopleList.add(stacy);
        peopleList.add(ashley);
        peopleList.add(matt);
        peopleList.add(matt2);
        peopleList.add(matt3);
        peopleList.add(matt4);
        peopleList.add(matt5);
        peopleList.add(matt6);
        peopleList.add(matt7);
        peopleList.add(matt8);
        peopleList.add(matt9);
        peopleList.add(matt10);
        peopleList.add(matt11);

        CustomerListAdapter adapter = new CustomerListAdapter(getActivity(), R.layout.customer_listview_layout, peopleList);
        customerNames.setAdapter(adapter);
        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

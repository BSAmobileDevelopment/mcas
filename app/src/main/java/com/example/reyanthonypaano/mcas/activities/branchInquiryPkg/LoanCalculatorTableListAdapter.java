package com.example.reyanthonypaano.mcas.activities.branchInquiryPkg;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.reyanthonypaano.mcas.R;

import java.util.ArrayList;

public class LoanCalculatorTableListAdapter extends ArrayAdapter<LoanCalculatorTable> {
    private static final String TAG = "LoanCalculatorTableListAdapter";

    private Context mContent;
    int mResource;

    /**
     * Default Constructor for LoanCalculatorTableListAdapter
     * @param context
     * @param resource
     * @param objects
     */

    public LoanCalculatorTableListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<LoanCalculatorTable> objects) {
        super(context, resource, objects);
        mContent = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //get LoanCalculator Data
        String Term = getItem(position).getTerm();
        String MA = getItem(position).getMA();
        String PNAmount = getItem(position).getPNAmount();

        //Create the LoanCalculator Object with the data
        LoanCalculatorTable loanCalculatorTable = new LoanCalculatorTable(Term,MA,PNAmount);

        LayoutInflater inflater = LayoutInflater.from(mContent);
        convertView = inflater.inflate(mResource,parent,false);

        TextView tv_Term = convertView.findViewById(R.id.tv_biP3Term_lv);
        TextView tv_MA = convertView.findViewById(R.id.tv_biP3Ma_lv);
        TextView tv_PNAmount = convertView.findViewById(R.id.tv_biP3PnAmnt_lv);

        tv_Term.setText(Term);
        tv_MA.setText(MA);
        tv_PNAmount.setText(PNAmount);

        return convertView;
    }
}

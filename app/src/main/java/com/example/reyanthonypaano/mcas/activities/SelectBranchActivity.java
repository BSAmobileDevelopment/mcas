package com.example.reyanthonypaano.mcas.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.reyanthonypaano.mcas.R;
import com.example.reyanthonypaano.mcas.adapters.CustomerListAdapter;
import com.example.reyanthonypaano.mcas.adapters.SpinAdapter;
import com.example.reyanthonypaano.mcas.model.Customers;

import java.util.ArrayList;

public class SelectBranchActivity extends AppCompatActivity {

    private Spinner branchSpinner;
    private SpinAdapter adapter;
    Dialog branchDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_branch);

        branchDialog = new Dialog(this);


        Customers john = new Customers("John","09778738263","12-20-1998");
        Customers steve = new Customers("Steve","09778738263","08-03-1987");
        Customers stacy = new Customers("Stacy","09778738263","11-15-2000");
        Customers ashley = new Customers("Ashley","09778738263","07-02-1999");

        ArrayList<Customers> peopleList = new ArrayList<>();
        peopleList.add(john);
        peopleList.add(steve);
        peopleList.add(stacy);
        peopleList.add(ashley);

        adapter = new SpinAdapter(SelectBranchActivity.this,
                R.layout.support_simple_spinner_dropdown_item, peopleList);
        branchSpinner = findViewById(R.id.branchSpinner);
        branchSpinner.setAdapter(adapter);

        branchSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                Customers user = adapter.getItem(position);
                // Here you can do the action you want to...
                Toast.makeText(SelectBranchActivity.this, "Name: " + user.getCustMame(),
                        Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }


}

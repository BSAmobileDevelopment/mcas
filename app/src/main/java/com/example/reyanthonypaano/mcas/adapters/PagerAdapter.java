package com.example.reyanthonypaano.mcas.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.reyanthonypaano.mcas.fragments.CustomersTabFragment;
import com.example.reyanthonypaano.mcas.fragments.InquiriesTabFragment;

public class PagerAdapter extends FragmentStatePagerAdapter{

    int tabCount;

    public PagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                CustomersTabFragment custTab = new CustomersTabFragment();
                return custTab;
            case 1:
                InquiriesTabFragment inqTab = new InquiriesTabFragment();
                return inqTab;
            default:
                return  null;


        }


    }

    @Override
    public int getCount() {
        return tabCount;
    }
}

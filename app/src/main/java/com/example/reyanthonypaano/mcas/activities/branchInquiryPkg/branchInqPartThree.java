package com.example.reyanthonypaano.mcas.activities.branchInquiryPkg;


import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.reyanthonypaano.mcas.R;
import com.example.reyanthonypaano.mcas.adapters.LoanCalculatorTableListAdapter;
import com.example.reyanthonypaano.mcas.model.LoanCalculatorTable;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class branchInqPartThree extends Fragment {
    private static final String TAG = "branchInqPartThree";
    public Spinner myBiSpnners;

    public static final branchInqPartThree newInstance(String message){
        branchInqPartThree fragment = new branchInqPartThree();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    public branchInqPartThree() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_branch_inq_part_three, container, false);

        ListView listViewTermMaPnAmnt = view.findViewById(R.id.lv_biP3TermMaPnamnt);

        LoanCalculatorTable a = new LoanCalculatorTable("John","09778738263","12-20-1998");
        LoanCalculatorTable b = new LoanCalculatorTable("Steve","09778738263","08-03-1987");
        LoanCalculatorTable c = new LoanCalculatorTable("Stacy","09778738263","11-15-2000");
        LoanCalculatorTable d = new LoanCalculatorTable("Ashley","09778738263","07-02-1999");
        LoanCalculatorTable e = new LoanCalculatorTable("Matt","09778738263","03-29-2001");
        LoanCalculatorTable f = new LoanCalculatorTable("Matt2","09778738263","03-29-2001");
        LoanCalculatorTable g = new LoanCalculatorTable("Matt3","09778738263","03-29-2001");
        LoanCalculatorTable h = new LoanCalculatorTable("Matt4","09778738263","03-29-2001");
        LoanCalculatorTable i = new LoanCalculatorTable("Matt5","09778738263","03-29-2001");
        LoanCalculatorTable j = new LoanCalculatorTable("Matt6","09778738263","03-29-2001");
        LoanCalculatorTable k = new LoanCalculatorTable("Matt7","09778738263","03-29-2001");
        LoanCalculatorTable l = new LoanCalculatorTable("Matt8","09778738263","03-29-2001");
        LoanCalculatorTable m = new LoanCalculatorTable("Matt9","09778738263","03-29-2001");
        LoanCalculatorTable n = new LoanCalculatorTable("Matt10","09778738263","03-29-2001");
        LoanCalculatorTable o = new LoanCalculatorTable("Matt11","09778738263","03-29-2001");

        ArrayList<LoanCalculatorTable> loanCalcList = new ArrayList<>();
        loanCalcList.add(a);
        loanCalcList.add(b);
        loanCalcList.add(c);
        loanCalcList.add(d);
        loanCalcList.add(e);
        loanCalcList.add(f);
        loanCalcList.add(g);
        loanCalcList.add(h);
        loanCalcList.add(i);
        loanCalcList.add(j);
        loanCalcList.add(k);
        loanCalcList.add(l);
        loanCalcList.add(m);
        loanCalcList.add(n);
        loanCalcList.add(o);

       LoanCalculatorTableListAdapter adapter = new LoanCalculatorTableListAdapter(getActivity(),
                R.layout.adpter_view_layout_bi_part3, loanCalcList);

        listViewTermMaPnAmnt.setAdapter(adapter);

        listViewTermMaPnAmnt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        TypedArray spnnrs = getResources().obtainTypedArray(R.array.mySpnnersId_partThree);
        changeSpinnersLayout(spnnrs,view);
        return view;
    }

    public void changeSpinnersLayout(TypedArray spinner_name, View v){
        for (int i = 0; i < spinner_name.length(); i++){
            myBiSpnners = v.findViewById(spinner_name.getResourceId(i,0));
            ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(),R.array.select, R.layout.spinner_layout);
            adapter.setDropDownViewResource(R.layout.spinner_layout);
            myBiSpnners.setAdapter(adapter);
        }
        spinner_name.recycle();
    }

}

package com.example.reyanthonypaano.mcas.model;

public class Customers {
    private String custMame;
    private String ecmastId;
    private String birthDate;

    public Customers(String custMame, String ecmastId, String birthDate) {
        this.custMame = custMame;
        this.ecmastId = ecmastId;
        this.birthDate = birthDate;
    }

    public String getCustMame() {
        return custMame;
    }

    public void setCustMame(String custMame) {
        this.custMame = custMame;
    }

    public String getEcmastId() {
        return ecmastId;
    }

    public void setEcmastId(String ecmastId) {
        this.ecmastId = ecmastId;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}


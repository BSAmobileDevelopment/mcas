package com.example.reyanthonypaano.mcas;


import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.util.Log;


public class NetworkJobService extends JobService {
    private static final String TAG = "NetworkJobService";

    private static boolean jobCancelled = false;
    private static int monitorNetworkForAMinutes = 20;//15 * 60 * 1000

    private static boolean isOnlineMode = false;

    public static boolean getOnlineMode() {
        return isOnlineMode;
    }

    public void setOnlineMode(boolean onlineMode) {
        this.isOnlineMode = onlineMode;
    }
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG, "Job Started");
        doBackgroundWork(params);
        return true;
    }

    private void doBackgroundWork(final JobParameters params){
        new Thread(new Runnable() {
            @Override
            public void run() {
                for( int i = 0; i < monitorNetworkForAMinutes; i++){
                    Log.d(TAG,"Run:" + i);
                    if( jobCancelled ){
                        if(getOnlineMode()){
                            NetworkJobScheduler.cancelJob(NetworkJobService.this);
                            NetworkJobScheduler.showPopWindow(NetworkJobService.this);
                        }
                        return;
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                Log.d(TAG, "Job Finished!");
                jobFinished(params, false);
                resetStaticVariables(false);
                NetworkJobScheduler.cancelJob(NetworkJobService.this);
                NetworkJobScheduler.scheduleJob(NetworkJobService.this);

            }
        }).start();

    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG, "Job Cancelled before completion!");
        boolean override = true;
        if(getOnlineMode()){ // checks if in online mode or offline mode
            if(Utils.checkNetworkInit(NetworkJobService.this)){
                override = false;
            }
        }

        jobCancelled = override;//true;
        return true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    public static void resetStaticVariables(boolean isReset){
        jobCancelled = isReset;
    }
}

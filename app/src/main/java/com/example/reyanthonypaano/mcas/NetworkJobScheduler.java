package com.example.reyanthonypaano.mcas;

import android.app.Activity;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

public class NetworkJobScheduler extends Activity {
    private static int schedulerId = 001;
    private static final String TAG = "NetworkJobScheduler";

    public static void scheduleJob(Context context) {
        NetworkJobService.resetStaticVariables(false);
        ComponentName componentName = new ComponentName(context, NetworkJobService.class);

        JobInfo info = new JobInfo.Builder(schedulerId, componentName)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPeriodic(15 * 60 * 1000) //15 * 60 * 1000
                .build();

        JobScheduler scheduler = (JobScheduler) context.getSystemService(JOB_SCHEDULER_SERVICE);

        scheduler.schedule(info);
        int resultCode = scheduler.schedule(info);
        if (resultCode == JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, "Job scheduling success");
        } else {
            Utils.checkNetwork(context,false);

        }
    }

    public static void cancelJob(Context context) {
        JobScheduler scheduler = (JobScheduler) context.getSystemService(JOB_SCHEDULER_SERVICE);


        if (scheduler.getAllPendingJobs().size() > 1) {
            scheduler.cancelAll();
            Log.d("NetworkJobService", "All Job cancelled");
        }
        scheduler.cancel(schedulerId);
        Log.d(TAG, "Job cancelled");
    }
    public static void showPopWindow(Context context){
        NetworkJobService.resetStaticVariables(false);
        Intent intent = new Intent(context, NetworkPopUp.class);
        context.startActivity(intent);
    }

    public static void reLogin(Context context){
        Intent intent = new Intent(context, Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static void repeatJobAfterCompletion(Context context){
        cancelJob(context);
        scheduleJob(context);
    }
}
